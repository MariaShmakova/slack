import React, { Component } from "react";
import ReactDOM from "react-dom";
import { composeWithDevTools } from "redux-devtools-extension";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter
} from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import { Provider, connect } from "react-redux";
import { createStore } from "redux";
import reducers from "./store/reducers";
import firebase from "./firebase";

import "./index.css";
import App from "./components/App";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import { setUser, clearUser } from "./store/actions/index";
import Spinner from "./components/Spinner";

class Root extends Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (!user) {
        this.props.history.push("/login");
        this.props.clearUser();
      } else {
        this.props.setUser(user);
        this.props.history.push("/");
      }
    });
  }

  render() {
    return this.props.isLoading ? (
      <Spinner />
    ) : (
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/" component={App} />
      </Switch>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.user.isLoading
});

const RootWithAuth = withRouter(
  connect(
    mapStateToProps,
    { setUser, clearUser }
  )(Root)
);
const store = createStore(reducers, composeWithDevTools());

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Spinner />
      <RootWithAuth />
    </Router>
  </Provider>,
  document.getElementById("root")
);
