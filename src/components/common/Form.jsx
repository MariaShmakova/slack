import React, { Component } from "react";
import Joi from "joi-browser";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      errors: {}
    };
  }

  handleSubmit = e => {
    if (e) e.preventDefault();

    const errors = this.validate();

    this.setState({ errors: errors || {} });
    if (errors) return;

    this.doSubmit();
  };

  handleChange = event => {
    const field = event.target;
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(field);
    if (errorMessage) errors[field.name] = errorMessage;
    else delete errors[field.name];

    const data = { ...this.state.data };
    data[field.name] = field.value;
    this.setState({ data, errors });
  };

  validate = () => {
    const options = { abortEarly: false };
    const { error } = Joi.validate(this.state.data, this.schema, options);
    if (!error) return null;

    let errors = {};

    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }

    return errors;
  };

  validateProperty = ({ name, value }) => {
    const schema = { [name]: this.schema[name] };
    const data = { [name]: value };
    const { error } = Joi.validate(data, schema);
    return error ? error.details[0].message : null;
  };

  handleFieldError = name => {
    return this.state.errors.hasOwnProperty(name) ? "error" : "";
  };

  renderErrors = errors => {
    let errorMessages = [];

    for (let name in errors) {
      if (!errors.hasOwnProperty(name)) continue;
      errorMessages.push(errors[name]);
    }
    return errorMessages.map((error, i) => <p key={i}>{error}</p>);
  };
}

export default Form;
