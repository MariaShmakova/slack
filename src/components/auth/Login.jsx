import React from "react";
import Joi from "joi-browser";
import Form from "../common/Form";
import {
  Grid,
  Form as FormUI,
  Segment,
  Button,
  Header,
  Message,
  Icon
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import firebase from "../../firebase";

class Login extends Form {
  state = {
    data: {
      email: "",
      password: ""
    },
    loading: false,
    errors: {},
    usersRef: firebase.database().ref("users")
  };

  schema = {
    email: Joi.string()
      .email()
      .required()
      .label("Email"),
    password: Joi.string()
      .regex(/^[a-zA-Z0-9]{3,30}$/)
      .label("Password")
  };

  doSubmit = () => {
    this.setState({ loading: true });
    const { email, password } = this.state.data;
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(signedInUser => {
        console.log("signedInUser: ", signedInUser);
      })
      .catch(err => {
        const errors = { ...this.state.errors };
        errors["create"] = err.message;
        this.setState({ errors, loading: false });
      })

      .finally(() => this.setState({ loading: false }));
  };

  render() {
    let { loading, errors } = this.state;
    let { email, password } = this.state.data;

    return (
      <Grid textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h1" icon color="orange" textAlign="center">
            Login to DevChat
            <Icon name="code" size="large" color="orange" />
          </Header>
          <FormUI onSubmit={this.handleSubmit} size="large">
            <Segment stacked>
              <FormUI.Input
                type="email"
                placeholder="Email"
                name="email"
                icon="mail"
                onChange={this.handleChange}
                className={this.handleFieldError("email")}
                value={email}
              />
              <FormUI.Input
                type="password"
                placeholder="Password"
                name="password"
                icon="lock"
                onChange={this.handleChange}
                className={this.handleFieldError("password")}
                value={password}
              />
              <Button disabled={loading} loading={loading} primary>
                Login
              </Button>
            </Segment>
          </FormUI>
          {Object.keys(errors).length > 0 && (
            <Message error>
              <h3>Error</h3>
              {this.renderErrors(errors)}
            </Message>
          )}
          <Message warning>
            <Icon name="help" />
            Don't have an account? <Link to="/register">Register</Link>
            &nbsp;instead.
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

export default Login;
