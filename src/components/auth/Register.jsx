import React from "react";
import Joi from "joi-browser";
import Form from "../common/Form";
import {
  Grid,
  Form as FormUI,
  Segment,
  Button,
  Header,
  Message,
  Icon
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import firebase from "../../firebase";
import md5 from "md5";

class Register extends Form {
  state = {
    data: {
      username: "",
      email: "",
      password: "",
      passwordConfirmation: ""
    },
    loading: false,
    errors: {},
    usersRef: firebase.database().ref("users")
  };

  schema = {
    username: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required()
      .label("Username"),
    email: Joi.string()
      .email()
      .required()
      .label("Email"),
    password: Joi.string()
      .regex(/^[a-zA-Z0-9]{3,30}$/)
      .label("Password"),
    passwordConfirmation: Joi.string()
      .required()
      .valid(Joi.ref("password"))
  };

  doSubmit = () => {
    this.setState({ loading: true });
    const { email, password } = this.state.data;
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(createdUser => {
        console.log("createdUser: ", createdUser);
        createdUser.user
          .updateProfile({
            displayName: this.state.data.username,
            photoURL: `http://gravatar.com/avatar/${md5(email)}?d=identicon`
          })
          .then(() => {
            this.saveUser(createdUser).then(() => console.log("user saved"));
          })
          .catch(err => {
            console.error(err);
            const errors = { ...this.state.errors };
            errors["save"] = err.message;
            this.setState({ errors, loading: false });
          });
      })
      .catch(err => {
        const errors = { ...this.state.errors };
        errors["create"] = err.message;
        this.setState({ errors });
      })
      .finally(() => this.setState({ loading: false }));
  };

  saveUser = createdUser => {
    console.log("saveUser: ", createdUser.user);
    return this.state.usersRef.child(createdUser.user.uid).set({
      name: createdUser.user.displayName,
      avatar: createdUser.user.photoURL
    });
  };

  render() {
    let { loading, errors, data } = this.state;
    let { username, email, password, passwordConfirmation } = data;

    return (
      <Grid textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h1" icon color="green" textAlign="center">
            Welcome to DevChat
            <Icon name="leaf" size="large" color="green" />
          </Header>
          <FormUI onSubmit={this.handleSubmit} size="large">
            <Segment stacked>
              <FormUI.Input
                fluid
                type="text"
                placeholder="Username"
                name="username"
                icon="user"
                onChange={this.handleChange}
                className={this.handleFieldError("username")}
                value={username}
              />

              <FormUI.Input
                type="email"
                placeholder="Email"
                name="email"
                icon="mail"
                onChange={this.handleChange}
                className={this.handleFieldError("email")}
                value={email}
              />
              <FormUI.Input
                type="password"
                placeholder="Password"
                name="password"
                icon="lock"
                onChange={this.handleChange}
                className={this.handleFieldError("password")}
                value={password}
              />
              <FormUI.Input
                type="password"
                placeholder="Password Confirmation"
                name="passwordConfirmation"
                icon="repeat"
                onChange={this.handleChange}
                className={this.handleFieldError("password")}
                value={passwordConfirmation}
              />
              <Button disabled={loading} loading={loading} primary>
                Register
              </Button>
            </Segment>
          </FormUI>
          {Object.keys(errors).length > 0 && (
            <Message error>
              <h3>Error</h3>
              {this.renderErrors(errors)}
            </Message>
          )}
          <Message warning>
            <Icon name="help" />
            Already signed up?&nbsp;<Link to="/login">Login here</Link>
            &nbsp;instead.
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

export default Register;
