import React from "react";
import { Loader, Dimmer } from "semantic-ui-react";
import { connect } from "react-redux";

const Spinner = ({ isLoading }) => {
  return (
    isLoading && (
      <Dimmer active>
        <Loader>Loading...</Loader>
      </Dimmer>
    )
  );
};

const mapStateToProps = state => ({
  isLoading: state.user.isLoading
});

export default connect(mapStateToProps)(Spinner);
