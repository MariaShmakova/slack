import React, { Component } from "react";
import firebase from "../../firebase";
import { connect } from "react-redux";
import { Icon, Menu } from "semantic-ui-react";
import {
  setCurrentChannel,
  setPrivateChannel
} from "../../store/actions/index";

class Starred extends Component {
  state = {
    user: this.props.currentUser,
    usersRef: firebase.database().ref("users"),
    starredChannels: [],
    activeChannel: ""
  };

  componentDidMount() {
    if (this.state.user) {
      this.addListener(this.state.user.uid);
    }
  }

  componentWillUnmount() {
    this.removeListeners();
  }

  removeListeners = () => {
    this.state.usersRef
      .child(this.state.user.uid)
      .child("starred")
      .off();
  };

  addListener = userId => {
    this.state.usersRef
      .child(userId)
      .child("starred")
      .on("child_added", snap => {
        let starredChannel = { id: snap.key, ...snap.val() };
        this.setState({
          starredChannels: [...this.state.starredChannels, starredChannel]
        });
      });

    this.state.usersRef
      .child(userId)
      .child("starred")
      .on("child_removed", snap => {
        const updatedStarredChannel = this.state.starredChannels.filter(
          channel => channel.id !== snap.key
        );
        this.setState({ starredChannels: updatedStarredChannel });
      });
  };

  setActiveChannel = channel => {
    this.setState({ activeChannel: channel.id });
  };

  changeChannel = channel => {
    this.setActiveChannel(channel);
    this.props.setCurrentChannel(channel);
    this.props.setPrivateChannel(false);
  };

  renderChannels = starredChannels => {
    const { activeChannel } = this.state;
    return (
      starredChannels.length > 0 &&
      starredChannels.map(channel => {
        return (
          <Menu.Item
            key={channel.id}
            onClick={() => this.changeChannel(channel)}
            style={{ opacity: "0.7" }}
            active={channel.id === activeChannel}
          >
            # {channel.name}
          </Menu.Item>
        );
      })
    );
  };

  render() {
    const { starredChannels } = this.state;
    return (
      <Menu.Menu className="menu">
        <Menu.Item>
          <span>
            <Icon name="star" /> STARRED
          </span>{" "}
          ({starredChannels.length})
        </Menu.Item>
        {this.renderChannels(starredChannels)}
      </Menu.Menu>
    );
  }
}

export default connect(
  null,
  { setCurrentChannel, setPrivateChannel }
)(Starred);
