import React, { Component } from "react";
import {
  Grid,
  Header,
  Icon,
  Dropdown,
  Image,
  Modal,
  Input,
  Button
} from "semantic-ui-react";
import AvatarEditor from "react-avatar-editor";
import firebase from "./../../firebase";

class UserPanel extends Component {
  state = {
    user: this.props.currentUser,
    modal: false,
    previewImage: "",
    croppedImage: "",
    blob: "",
    uploadCroppedImage: "",
    storageRef: firebase.storage().ref(),
    userRef: firebase.auth().currentUser,
    usersRef: firebase.database().ref("users"),
    metadata: {
      contentType: "image/jpeg"
    }
  };

  openModal = () => this.setState({ modal: true });

  closeModal = () => this.setState({ modal: false });

  getDropdownOptions = () => {
    return [
      {
        key: "user",
        text: (
          <span>
            Signed in as <strong>{this.state.user.displayName}</strong>
          </span>
        ),
        disabled: true
      },
      {
        key: "avatar",
        text: <span onClick={this.openModal}>Change Avatar</span>
      },
      {
        key: "signout",
        text: <span onClick={this.handleSignOut}>Sign Out</span>
      }
    ];
  };

  handleSignOut = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        console.log("singOut");
      });
  };

  handleChangePreviewImage = event => {
    const file = event.target.files[0];
    const reader = new FileReader();

    if (file) {
      reader.readAsDataURL(file);
      reader.addEventListener("load", () => {
        this.setState({
          previewImage: reader.result
        });
      });
    }
  };

  handleCropImage = () => {
    if (this.avatarEditor) {
      this.avatarEditor.getImageScaledToCanvas().toBlob(blob => {
        let imageUrl = URL.createObjectURL(blob);
        this.setState({
          croppedImage: imageUrl,
          blob
        });
      });
    }
  };

  uploadedCroppedAvatar = () => {
    const { storageRef, user, blob, metadata } = this.state;

    storageRef
      .child(`avatars/users/${user.uid}`)
      .put(blob, metadata)
      .then(snap => {
        snap.ref.getDownloadURL().then(dowloadUrl => {
          this.setState({ uploadCroppedImage: dowloadUrl }, () =>
            this.changeAvatar()
          );
        });
      });
  };

  changeAvatar = () => {
    this.state.userRef
      .updateProfile({
        photoURL: this.state.uploadCroppedImage
      })
      .then(() => {
        console.log("PhotoUrl updated");
        this.closeModal();
      })
      .catch(err => console.error(err));

    this.state.usersRef
      .child(this.state.user.uid)
      .update({
        avatar: this.state.uploadCroppedImage
      })
      .then(() => console.log("User avatar updated"))
      .catch(err => console.error(err));
  };

  render() {
    const { user, modal, previewImage, croppedImage } = this.state;

    return (
      <Grid>
        <Grid.Column>
          <Grid.Row style={{ padding: "1.2em", margin: 0 }}>
            <Header as="h2" inverted floated="left">
              <Icon name="code" />
              <Header.Content>DevChat</Header.Content>
            </Header>
            <Header style={{ padding: "0.25em" }} as="h4" inverted>
              <Dropdown
                trigger={
                  <span>
                    <Image src={user.photoURL} spaced="right" avatar />
                    {user.displayName}
                  </span>
                }
                options={this.getDropdownOptions()}
              />
            </Header>
          </Grid.Row>
        </Grid.Column>
        <Modal basic open={modal} onClose={this.closeModal}>
          <Modal.Header />
          <Modal.Content>
            <Input
              type="file"
              name="previewImage"
              label="New Avatar"
              fluid
              onChange={this.handleChangePreviewImage}
            />
            <Grid centered stackable columns={2}>
              <Grid.Row centered>
                <Grid.Column className="ui center aligned grid">
                  {/* Image preview */}
                  {previewImage && (
                    <AvatarEditor
                      ref={node => (this.avatarEditor = node)}
                      image={previewImage}
                      width={120}
                      height={120}
                      border={50}
                      scale={1.2}
                    />
                  )}
                </Grid.Column>
                <Grid.Column>
                  {/* Cropped Image preview */}
                  {croppedImage && (
                    <Image
                      src={croppedImage}
                      width={100}
                      height={100}
                      style={{ margin: "3.5em auto" }}
                    />
                  )}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Modal.Content>
          <Modal.Actions>
            {croppedImage && (
              <Button
                color="green"
                inverted
                onClick={this.uploadedCroppedAvatar}
              >
                <Icon name="save" /> Change Avatar
              </Button>
            )}
            <Button color="green" inverted onClick={this.handleCropImage}>
              <Icon name="image" /> Preview
            </Button>
            <Button color="red" inverted onClick={this.closeModal}>
              <Icon name="remove" /> Cancel
            </Button>
          </Modal.Actions>
        </Modal>
      </Grid>
    );
  }
}

export default UserPanel;
