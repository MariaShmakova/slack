import React from "react";
import {
  Menu,
  Icon,
  Modal,
  Form as FormUI,
  Input,
  Button,
  Message,
  Label
} from "semantic-ui-react";
import Joi from "joi-browser";
import Form from "./../common/Form";
import firebase from "../../firebase";
import { connect } from "react-redux";
import {
  setCurrentChannel,
  setPrivateChannel
} from "../../store/actions/index";

class Channels extends Form {
  state = {
    channel: null,
    channels: [],
    activeChannel: "",
    modal: false,
    data: {
      channelName: "",
      channelDetails: ""
    },
    errors: [],
    user: this.props.currentUser,
    channelsRef: firebase.database().ref("channels"),
    messagesRef: firebase.database().ref("messages"),
    typingRef: firebase.database().ref("typing"),
    notifications: [],
    firstLoad: true
  };

  schema = {
    channelName: Joi.string()
      .required()
      .label("Name of Channel"),
    channelDetails: Joi.string()
      .required()
      .label("Channel Details")
  };

  componentDidMount() {
    this.addListener();
  }

  componentWillUnmount() {
    this.removeListener();
  }

  addListener = () => {
    let loadedChannels = [];

    this.state.channelsRef.on("child_added", snap => {
      loadedChannels.push(snap.val());
      this.setState({ channels: loadedChannels }, () => this.setFirstChannel());
      this.addNotificationListener(snap.key);
    });
  };

  removeListener = () => {
    this.state.channelsRef.off();
    this.state.channels.forEach(channel => {
      this.state.messagesRef.child(channel.id).off();
    });
  };

  addNotificationListener = channelId => {
    this.state.messagesRef.child(channelId).on("value", snap => {
      if (this.state.channel) {
        this.handleNotifications(
          channelId,
          this.state.channel.id,
          this.state.notifications,
          snap
        );
      }
    });
  };

  handleNotifications = (channelId, currentChannelId, notifications, snap) => {
    let lastTotal = 0;

    let index = notifications.findIndex(
      notification => notification.id === channelId
    );

    if (index !== -1) {
      if (channelId !== currentChannelId) {
        lastTotal = notifications[index].total;

        if (snap.numChildren() - lastTotal > 0) {
          notifications[index].count = snap.numChildren() - lastTotal;
        }

        notifications[index].lastKnowTotal = snap.numChildren();
      }
    } else {
      notifications.push({
        id: channelId,
        total: snap.numChildren(),
        lastKnowTotal: snap.numChildren(),
        count: 0
      });
    }

    this.setState({ notifications });
  };

  setFirstChannel = () => {
    const { channels } = this.state;
    if (this.state.firstLoad && channels.length > 0) {
      const firstChannel = channels[0];
      this.props.setCurrentChannel(firstChannel);
      this.setActiveChannel(firstChannel);
      this.setState({ channel: firstChannel });
    }

    this.setState({ firstLoad: false });
  };

  setActiveChannel = channel => {
    this.setState({ activeChannel: channel.id });
  };

  doSubmit = () => {
    const { channelsRef, data, user } = this.state;

    const key = channelsRef.push().key;

    const newChannel = {
      id: key,
      name: data.channelName,
      details: data.channelDetails,
      createdBy: {
        user: user.displayName,
        avatar: user.photoURL
      }
    };

    channelsRef
      .child(key)
      .update(newChannel)
      .then(() => {
        this.closeModal();
        this.setState({ data: { channelName: "", channelDetails: "" } });
      })
      .catch(err => {
        let errors = { ...this.state.error };
        errors.push({ add: err });
        this.setState({ errors });
      });
  };

  changeChannel = channel => {
    this.typingRemove(this.state.channel.id, this.state.user.uid);
    this.props.setCurrentChannel(channel);
    this.setActiveChannel(channel);
    this.props.setPrivateChannel(false);
    this.setState({ channel });
    this.clearNotifications();
  };

  typingRemove = (channelId, userId) => {
    this.state.typingRef
      .child(channelId)
      .child(userId)
      .remove();
  };

  clearNotifications = () => {
    const index = this.state.notifications.findIndex(
      notification => notification.id === this.state.channel.id
    );

    if (index !== -1) {
      let updatedNotifications = [...this.state.notifications];
      updatedNotifications[index].total =
        updatedNotifications[index].lastKnowTotal;
      updatedNotifications[index].count = 0;
      this.setState({ notifications: updatedNotifications });
    }
  };

  renderChannels = () => {
    const { channels, activeChannel } = this.state;
    return (
      channels.length > 0 &&
      channels.map(channel => {
        return (
          <Menu.Item
            key={channel.id}
            onClick={() => this.changeChannel(channel)}
            style={{ opacity: "0.7" }}
            active={channel.id === activeChannel}
          >
            {this.getNotificationCount(channel) && (
              <Label color="red">{this.getNotificationCount(channel)}</Label>
            )}
            # {channel.name}
          </Menu.Item>
        );
      })
    );
  };

  getNotificationCount = channel => {
    const index = this.state.notifications.findIndex(
      notification => notification.id === channel.id
    );

    if (index !== -1) {
      const count = this.state.notifications[index].count;
      return count > 0 ? count : false;
    }
    return false;
  };

  closeModal = () => this.setState({ modal: false });

  openModal = () => this.setState({ modal: true });

  render() {
    const { channels, modal, errors } = this.state;
    const { channelName, channelDetails } = this.state.data;

    return (
      <React.Fragment>
        <Menu.Menu className="menu">
          <Menu.Item>
            <span>
              <Icon name="exchange" /> CHANNELS
            </span>{" "}
            ({channels.length})
            <Icon name="add" onClick={this.openModal} />
          </Menu.Item>
          {this.renderChannels()}
        </Menu.Menu>

        {/* Add Channel Modal */}
        <Modal open={modal} onClose={this.closeModal}>
          <Modal.Header>Add a Channel</Modal.Header>
          <Modal.Content>
            <FormUI onSubmit={this.handleSubmit}>
              <FormUI.Field>
                <Input
                  name="channelName"
                  label="Name of Channel"
                  onChange={this.handleChange}
                  className={this.handleFieldError("channelName")}
                  value={channelName}
                />
              </FormUI.Field>
              <FormUI.Field>
                <Input
                  name="channelDetails"
                  label="About the Channel"
                  onChange={this.handleChange}
                  className={this.handleFieldError("channelDetails")}
                  value={channelDetails}
                />
              </FormUI.Field>
            </FormUI>
            {Object.keys(errors).length > 0 && (
              <Message error>
                <h3>Error</h3>
                {this.renderErrors(errors)}
              </Message>
            )}
          </Modal.Content>
          <Modal.Actions>
            <Button color="green" inverted onClick={this.handleSubmit}>
              <Icon name="checkmark" /> Add
            </Button>
            <Button color="red" inverted onClick={this.closeModal}>
              <Icon name="remove" /> Cancel
            </Button>
          </Modal.Actions>
        </Modal>
      </React.Fragment>
    );
  }
}

export default connect(
  null,
  { setCurrentChannel, setPrivateChannel }
)(Channels);
