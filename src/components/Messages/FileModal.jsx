import React, { Component } from "react";
import mime from "mime-types";
import { Modal, Icon, Input, Button } from "semantic-ui-react";

class FileModal extends Component {
  state = {
    file: null,
    authorized: ["image/png", "image/jpeg"]
  };

  addFile = event => {
    const file = event.target.files[0];

    if (file && this.isAuthorized(file.name)) {
      this.setState({ file });
    }
  };

  isAuthorized = filename =>
    this.state.authorized.includes(mime.lookup(filename));

  sendFile = () => {
    const { file } = this.state;
    const { uploadFile, closeModal } = this.props;

    if (file !== null) {
      uploadFile(file, { contentType: mime.lookup(file.name) });
      this.setState({ file: null });
      closeModal();
    }
  };

  render() {
    const { modal, closeModal } = this.props;
    return (
      <Modal basic open={modal} onClose={closeModal}>
        <Modal.Header>Select an image file</Modal.Header>
        <Modal.Content>
          <Input
            fluid
            label="File types: jpg, png"
            name="file"
            type="file"
            onChange={this.addFile}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button inverted color="green" onClick={this.sendFile}>
            <Icon name="checkmark" /> Select
          </Button>
          <Button inverted color="red" onClick={closeModal}>
            <Icon name="remove" /> Cancel
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default FileModal;
