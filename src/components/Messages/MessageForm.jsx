import React from "react";
import { Segment, Input, Button, Form as FormUI } from "semantic-ui-react";
import Joi from "joi-browser";
import uuidv4 from "uuid/v4";
import firebase from "../../firebase";
import { Picker, emojiIndex } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";

import Form from "../common/Form";
import ModalFile from "./FileModal";
import ProgressBar from "./ProgressBar";

class MessageForm extends Form {
  state = {
    user: this.props.currentUser,
    channel: this.props.currentChannel,
    data: {
      message: ""
    },
    errors: {},
    loading: false,
    modalFile: false,
    uploadState: "",
    uploadTask: null,
    storageRef: firebase.storage().ref(),
    typingRef: firebase.database().ref("typing"),
    percentUploaded: 0,
    emojiPicker: false
  };

  componentWillUnmount() {
    if (this.state.uploadTask !== null) {
      this.state.uploadTask.cancel();
      this.setState({ uploadTask: null });
    }
  }

  modalFileOpen = () => this.setState({ modalFile: true });
  modalFileClose = () => this.setState({ modalFile: false });

  schema = {
    message: Joi.string()
      .required()
      .label("Messages")
  };

  doSubmit = () => {
    const { getMessagesRef } = this.props;
    const { channel, user } = this.state;

    this.setState({ loading: true });
    getMessagesRef()
      .child(channel.id)
      .push()
      .set(this.createMessage())
      .then(() => {
        const data = { ...this.state.data, message: "" };
        this.setState({ data, errors: {} });
        this.typingRemove(channel.id, user.uid);
      })
      .catch(err => {
        let errors = { ...this.state.errors };
        errors["create"] = err.message;
        this.setState({ errors });
      })
      .finally(() => this.setState({ loading: false }));
  };

  createMessage = (fileUrl = null) => {
    const { data, user } = this.state;
    let message = {
      timestamp: firebase.database.ServerValue.TIMESTAMP,
      user: {
        id: user.uid,
        name: user.displayName,
        avatar: user.photoURL
      }
    };

    if (fileUrl !== null && !data.message) {
      message.image = fileUrl;
    } else {
      message.content = data.message;
    }

    return message;
  };

  getPath = () => {
    if (this.props.isPrivateChannel) {
      return `chat/private/${this.state.channel.id}`;
    }
    return "chat/public";
  };

  uploadFile = (file, metadata) => {
    const storageFilePath = `${this.getPath()}/${uuidv4()}.jpg`;

    this.setState(
      {
        uploadState: "uploading",
        uploadTask: this.state.storageRef
          .child(storageFilePath)
          .put(file, metadata)
      },
      () => {
        this.state.uploadTask.on(
          "state_changed",
          snap => {
            const percentUploaded = Math.round(
              (snap.bytesTransferred / snap.totalBytes) * 100
            );
            this.props.isProgressBarVisible(percentUploaded);
            this.setState({ percentUploaded });
          },
          err => {
            let errors = { ...this.state.errors };
            errors["uploadImageToStorage"] = err.message;
            this.setState({ errors, uploadState: "error", uploadTask: null });
          },
          () => {
            this.state.uploadTask.snapshot.ref
              .getDownloadURL()
              .then(fileUrl => {
                this.sendFileMessage(fileUrl);
              })
              .catch(err => {
                let errors = { ...this.state.errors };
                errors["getDownloadURL"] = err.message;
                this.setState({
                  errors,
                  uploadState: "error",
                  uploadTask: null
                });
              });
          }
        );
      }
    );
  };

  sendFileMessage = fileUrl => {
    const ref = this.props.getMessagesRef();
    ref
      .child(this.state.channel.id)
      .push()
      .set(this.createMessage(fileUrl))
      .then(() => {
        this.setState({ uploadState: "done" });
      })
      .catch(err => {
        let errors = { ...this.state.errors };
        errors["addImageMessage"] = err.message;
        this.setState({
          errors,
          uploadState: "error",
          uploadTask: null
        });
      });
  };

  typingSet = (channelId, user) => {
    this.state.typingRef
      .child(channelId)
      .child(user.uid)
      .set(user.displayName);
  };

  typingRemove = (channelId, userId) => {
    this.state.typingRef
      .child(channelId)
      .child(userId)
      .remove();
  };

  handleKeyDown = event => {
    event.stopPropagation();
    if (event.ctrlKey && event.keyCode === 13) {
      event.preventDefault();
      this.handleSubmit();
    }

    const { data, channel, user } = this.state;
    if (data.message) {
      this.typingSet(channel.id, user);
    } else {
      this.typingRemove(channel.id, user.uid);
    }
  };

  handleEmojiPicker = event => {
    event.preventDefault();
    this.setState({ emojiPicker: !this.state.emojiPicker });
  };

  handleAddEmoji = emoji => {
    const { message } = this.state.data;
    const newMessage = this.colonToUnicode(`${message} ${emoji.colons}`);
    let data = { ...this.state.data, message: newMessage };

    this.setState({ data, emojiPicker: false });
    setTimeout(() => this.messageInputRef.focus(), 0);
  };

  colonToUnicode = message => {
    return message.replace(/:[A-Za-z0-9_+-]+:/g, x => {
      x = x.replace(/:/g, "");
      let emoji = emojiIndex.emojis[x];
      if (typeof emoji !== "undefined") {
        let unicode = emoji.native;
        if (typeof unicode !== "undefined") {
          return unicode;
        }
      }
      x = ":" + x + ":";
      return x;
    });
  };

  handleFocus = event => {
    const val = event.target.value;
    event.target.value = "";
    event.target.value = val;
  };

  render() {
    // prettier-ignore
    const { loading, data, modalFile, percentUploaded,uploadState, emojiPicker } = this.state;
    return (
      <Segment className="message__form">
        {emojiPicker && (
          <Picker
            set="apple"
            className="emojipicker"
            title="Pick emoji"
            emoji="point_up"
            onSelect={this.handleAddEmoji}
          />
        )}
        <FormUI onSubmit={this.handleSubmit}>
          <Input
            fluid
            name="message"
            placeholder="Write your message"
            label={
              <Button
                icon={emojiPicker ? "close" : "add"}
                content={emojiPicker ? "Close" : null}
                onClick={this.handleEmojiPicker}
              />
            }
            ref={node => (this.messageInputRef = node)}
            labelPosition="left"
            onKeyDown={this.handleKeyDown}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            style={{ marginBottom: "0.7em" }}
            className={this.handleFieldError("message")}
            value={data.message}
          />
          <Button.Group icon widths="2">
            <Button
              icon="edit"
              color="orange"
              labelPosition="left"
              content="Add Reply"
              onClick={this.handleSubmit}
              disabled={loading}
            />
            <Button
              icon="cloud"
              color="teal"
              disabled={uploadState === "uploading"}
              labelPosition="right"
              content="Upload media"
              onClick={this.modalFileOpen}
            />
          </Button.Group>
        </FormUI>
        <ModalFile
          modal={modalFile}
          closeModal={this.modalFileClose}
          uploadFile={this.uploadFile}
        />
        <ProgressBar percent={percentUploaded} uploadState={uploadState} />
      </Segment>
    );
  }
}

export default MessageForm;
