import React from "react";
import { Progress } from "semantic-ui-react";

const ProgressBar = ({ percent, uploadState }) => {
  return (
    uploadState && (
      <Progress
        percent={percent}
        size="medium"
        indicating
        progress
        inverted
        className="progress__bar"
      />
    )
  );
};

export default ProgressBar;
