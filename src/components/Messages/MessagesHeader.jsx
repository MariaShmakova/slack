import React, { Component } from "react";
import { Segment, Header, Input, Icon } from "semantic-ui-react";

class MessagesHeader extends Component {
  state = {};
  render() {
    const {
      channelName,
      numUniqueUsers,
      handleSearchChange,
      searchLoading,
      isPrivateChannel,
      handleStar,
      isChannelStarred
    } = this.props;
    return (
      <Segment clearing>
        {/* Channel Title */}
        <Header
          as="h2"
          floated="left"
          fluid="true"
          style={{ marginBottom: "0" }}
        >
          <span>
            {channelName}
            {!isPrivateChannel && (
              <Icon
                onClick={handleStar}
                color={isChannelStarred ? "yellow" : "black"}
                name={isChannelStarred ? "star" : "star outline"}
              />
            )}
          </span>
          <Header.Subheader>{numUniqueUsers}</Header.Subheader>
        </Header>

        {/* Channel Search */}
        <Header floated="right">
          <Input
            loading={searchLoading}
            onChange={handleSearchChange}
            name="searchTerm"
            icon="search"
            placeholder="Search messages"
            size="mini"
          />
        </Header>
      </Segment>
    );
  }
}

export default MessagesHeader;
