import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCIQx5eNieJ_3ns2NW3WQO5Br3O3aNCk8A",
    authDomain: "react-slack-clone-96405.firebaseapp.com",
    databaseURL: "https://react-slack-clone-96405.firebaseio.com",
    projectId: "react-slack-clone-96405",
    storageBucket: "react-slack-clone-96405.appspot.com",
    messagingSenderId: "284589856150",
    appId: "1:284589856150:web:b7cd6eb91a4b1783"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;